export default interface Checklist {
  id?: number;
  name?: string;
  amount?: number;
  minimum?: number;
  unit?: string;
}

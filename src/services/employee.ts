import type Employee from "@/types/employee";
import http from "./axios";
function getEmployee() {
  return http.get("/employees");
}

function getEmployeeByID(id: number) {
  return http.get(`/employees/${id}`);
}

function saveEmployee(employee: Employee) {
  return http.post("/employees", employee);
}

function updateEmployee(id: number, employee: Employee) {
  return http.patch(`/employees/${id}`, employee);
}

function daleteEmployee(id: number) {
  return http.delete(`/employees/${id}`);
}
export default {
  getEmployee,
  updateEmployee,
  daleteEmployee,
  saveEmployee,
  getEmployeeByID,
};

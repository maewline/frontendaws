import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useSelectStore } from "@/stores/selectMenu";
import { useFoodStore } from "@/stores/foods";

export const usemanagePopupStore = defineStore("popupHandle", () => {
  const foodStore = useFoodStore();
  const selected = useSelectStore();
  const popupMenuIsShow = ref(false);
  const popupEditOrderIsShow = ref(false);
  const popupTable = ref(false);
  const scanQR = ref(false);
  const option = ref(0);
  const nameTable = ref("");
  // const sum = ref<number>(foodStore.foods[selected.selectedID].price);
  const sum = ref<number>(0);
  const piece = ref<number>(1);
  const priceNoOption = ref();
  // piece.value * foodStore.foods[selected.selectedID].price;
  piece.value * 1;
  const switchPopup = () => {
    popupMenuIsShow.value = true;
    console.log(popupMenuIsShow);
  };
  const switchOffPopup = () => {
    popupMenuIsShow.value = false;
    piece.value = 1;
  };
  return {
    popupMenuIsShow,
    switchPopup,
    switchOffPopup,
    piece,
    sum,
    option,
    popupEditOrderIsShow,
    popupTable,
    scanQR,
    nameTable,
  };
});
